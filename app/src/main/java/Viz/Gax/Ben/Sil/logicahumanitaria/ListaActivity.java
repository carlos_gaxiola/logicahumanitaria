package Viz.Gax.Ben.Sil.logicahumanitaria;

/*import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.estados.Estado;
import com.example.estados.ProyectoEstado;*/
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
/*import android.support.v7.app.AppCompatActivity;*/
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListaActivity extends ListActivity {

    private EstadoAdapter dbEstados;
    private ProcesosPHP php = new ProcesosPHP();
    private ArrayList<Estado> estados;
    private MyArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Button btnNuevo = findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        php.setContext(ListaActivity.this);

        dbEstados = new EstadoAdapter(this);
        dbEstados.openDatabase();
        estados = dbEstados.allEstados();

        adapter = new MyArrayAdapter(this, R.layout.activity_estado, estados);
        setListAdapter(adapter);
    }

    class MyArrayAdapter extends ArrayAdapter<Estado> {

        Context context;
        int textViewResourceId;
        ArrayList<Estado> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estado> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup){

            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);

            TextView lblNombre = view.findViewById(R.id.lblNombreEstado);
            lblNombre.setTextColor(Color.BLACK);
            lblNombre.setText(objects.get(position).getNombre());

            Button borrar = view.findViewById(R.id.btnBorrar);
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(ListaActivity.this);
                    dialog.setTitle("Borrar estado");
                    String nombre = estados.get(position).getNombre();
                    dialog.setMessage("¿Se borrara el estado " + nombre + "?");
                    dialog.setCancelable(false);
                    dialog.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Estado estado = objects.get(position);
                            php.borrarEstado(estado, delResponse(estado), errorResponse());
                        }
                    });
                    dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { } });
                    dialog.show();
                }
            });

            Button modificar = view.findViewById(R.id.btnModificar);
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estado", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });

            return view;
        }

        public void setObjects (ArrayList<Estado> estados) {
            this.objects = estados;
        }
    }

    private void mensajeCorto (String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private Response.Listener<JSONObject> delResponse (final Estado estado) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray estados = response.getJSONArray("estados");
                    mensaje("Error al borrar estado.");
                } catch (Exception ex) {
                    dbEstados.deleteEstado(estado.get_ID());
                    estados = dbEstados.allEstados();
                    adapter.setObjects(estados);
                    adapter.notifyDataSetChanged();
                }
            }
        };
    }

    private Response.ErrorListener errorResponse() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mensaje("Ocurrio un error.");
            }
        };
    }

    private void mensaje (String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
