package Viz.Gax.Ben.Sil.logicahumanitaria;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class EstadoAdapter {
    Context context;
    DbEstados mDbHelper;
    SQLiteDatabase db;

    String[] columnsToRead = new String[]{
            DefinirTabla.Estado._ID,
            DefinirTabla.Estado.COLUMN_NAME_NOMBRE,
            DefinirTabla.Estado.COLUMN_NAME_ESTATUS
    };
    public EstadoAdapter (Context context){
        this.context = context;
        mDbHelper = new DbEstados(this.context);
    }
    public void openDatabase(){
        db = mDbHelper.getWritableDatabase();
    }
    public long insertEstado(Estado c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_ESTATUS, c.getEstatus());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);
        //regresa el id insertado
    }

    public long updateEstado(Estado c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.Estado.COLUMN_NAME_ESTATUS, c.getEstatus());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estado.TABLE_NAME , values,
                DefinirTabla.Estado._ID + " = " + id,null);
    }
    public int deleteEstado(int id){
        return db.delete(DefinirTabla.Estado.TABLE_NAME,
                DefinirTabla.Estado._ID + "=?",
                new String[]{ String.valueOf(id) });
    }
    private Estado readEstado(Cursor cursor){
        Estado c = new Estado();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setEstatus(cursor.getInt(2));
        return c;
    }
    public Estado getEstado(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnsToRead,

                DefinirTabla.Estado._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estado estado = readEstado(c);
        c.close();
        return estado;
    }
    public ArrayList<Estado> allEstados(){
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME,columnsToRead, null, null, null, null, null);
        ArrayList<Estado> estados = new ArrayList<Estado>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estado c = readEstado(cursor);
            estados.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return estados;
    }
    public void close(){
        mDbHelper.close();
    }
    /*Esta funcion agrega todos los estados*/
    public void addEstados (ArrayList<Estado> estados) {
        for (Estado estado : estados) {
            long id = 0;
            id = this.insertEstado(estado);
        }
    }
}
