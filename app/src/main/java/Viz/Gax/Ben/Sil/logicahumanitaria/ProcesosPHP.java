package Viz.Gax.Ben.Sil.logicahumanitaria;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

public class ProcesosPHP {

    private RequestQueue queue;
    private JsonObjectRequest request;
    private String serverURL = "http://mariselle-server.000webhostapp.com/WS-Estados/";

    public void setContext (Context context) {
        this.queue = Volley.newRequestQueue(context);
    }

    public String urlGET (String file, Estado estado) {
        String url = serverURL + file;
        String sep = "?";
        if (estado != null) {
            if (!estado.getNombre().equals("")) {
                url += sep + "nombre=" + estado.getNombre();
                sep = "&";
            }
            if (estado.getEstatus() != 0) {
                url += sep + "estatus=" + estado.getEstatus();
                sep = "&";
            }
            if (!estado.getIdMovil().equals("")) {
                url += sep + "idMovil=" + estado.getIdMovil();
                sep = "&";
            }
            if (estado.get_ID() != 0) {
                url += sep + "ID=" + estado.get_ID();
                sep = "&";
            }
        }
        url = url.replace(" ", "%20");
        return url;
    }

    public void insertarEstado (Estado estado, Response.Listener<JSONObject> response,
            Response.ErrorListener error) {
        String url = urlGET("wsAgregar.php", estado);
        url = url.replace(" ", "%20");
        request = new JsonObjectRequest(Request.Method.GET, url, null, response, error);
        queue.add(request);
    }

    public void actualizarEstado (Estado estado, Response.Listener<JSONObject> response,
            Response.ErrorListener error) {
        String url = urlGET("wsModificar.php", estado);
        url = url.replace(" ", "%20");
        request = new JsonObjectRequest(Request.Method.GET, url, null, response, error);
        queue.add(request);
    }

    public void borrarEstado (Estado estado, Response.Listener<JSONObject> response,
            Response.ErrorListener error) {
        String url = urlGET("wsBorrar.php",  estado);
        request = new JsonObjectRequest(Request.Method.GET, url,
                null, response, error);
        queue.add(request);
    }

    public void cargarTodos (ArrayList<Estado> estado, String idMovil,
             Response.Listener<JSONObject> response, Response.ErrorListener error) {
        String url = urlGET("wsMostrarTodos.php", null) +
                "&idMovil=" + idMovil;
        request = new JsonObjectRequest(Request.Method.GET, url, null, response, error);
        queue.add(request);
    }
}
