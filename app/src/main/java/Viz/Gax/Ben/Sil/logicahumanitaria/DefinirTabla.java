package Viz.Gax.Ben.Sil.logicahumanitaria;

import android.provider.BaseColumns;

public final class DefinirTabla {
    public DefinirTabla(){}
    public static abstract class Estado implements BaseColumns {
        public static final String TABLE_NAME = "estados";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_ESTATUS = "estatus";
        public static final String COLUMN_NAME_IDMOVIL = "idmovil";
    }
}
