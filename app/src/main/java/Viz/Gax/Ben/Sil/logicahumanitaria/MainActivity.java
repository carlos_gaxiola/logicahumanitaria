/*
	Proyecto creado por
	Mariselle Vizcarra Ramos
	Carlos Alberto Hernandez Gaxiola
	Cesar Benitez Plazola
	Jose Alfredo Silva
*/
package Viz.Gax.Ben.Sil.logicahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import Viz.Gax.Ben.Sil.logicahumanitaria.Estado;
import Viz.Gax.Ben.Sil.logicahumanitaria.EstadoAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Estado estado;
    private Button btnGuardar, btnListar, btnLimpiar, btnCerrar;
    private EditText txtNombre;
    private ProcesosPHP php;
    private EstadoAdapter dbEstados;
    private String idMovil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setListeners();
        if (esPrimeraVez())
            sincronizarDB();
    }

    private void sincronizarDB() {
        if (!isNetworkingAvailable())
            mensaje("Para sincronizar los datos, se requiere conexión a internet.");
        else {
            ArrayList<Estado> estados = new ArrayList<>();
            php.cargarTodos(estados, idMovil, sincResponse(), errorResponse());
        }
    }

    private Response.Listener<JSONObject> sincResponse() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray estadosWS = response.getJSONArray("estados");
                    ArrayList<Estado> estadosLocal;
                    if (estadosWS.length() == 0) {
                        estadosLocal = getEstadosPrimeraVez();
                        for (Estado estadoTemp : estadosLocal) {
                            estado = estadoTemp;
                            php.insertarEstado(estado, insertReponse(false), errorResponse());
                        }
                    } else {
                        estadosLocal = new ArrayList<>();
                        for (int i = 0; i < estadosWS.length(); i++) {
                            Estado estadoTemp = new Estado();
                            JSONObject estadoWS = estadosWS.getJSONObject(i);
                            estadoTemp.set_ID(estadoWS.getInt("ID"));
                            estadoTemp.setNombre(estadoWS.getString("nombre"));
                            estadoTemp.setEstatus(estadoWS.getInt("estatus"));
                            estadoTemp.setIdMovil(estadoWS.getString("idMovil"));
                            estadosLocal.add(estadoTemp);
                        }
                        dbEstados.addEstados(estadosLocal);
                    }
                } catch (Exception ex) {
                    mensaje("Error al obtener los dats");
                }
            }
        };
    }

    private void setListeners() {
        btnGuardar.setOnClickListener(this);
        btnLimpiar.setOnClickListener(this);
        btnListar.setOnClickListener(this);
        btnCerrar.setOnClickListener(this);
    }

    private void initComponents () {
        txtNombre = findViewById(R.id.txtNombre);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnListar = findViewById(R.id.btnListar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        dbEstados = new EstadoAdapter(this);
        dbEstados.openDatabase();
        php = new ProcesosPHP();
        php.setContext(this);
        idMovil = Device.getSecureId(this);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK == resultCode) {
            limpiar();
            estado = (Estado) data.getSerializableExtra("estado");
            if (estado == null) mensaje("Error al obtener el estado.");
            else txtNombre.setText(estado.getNombre());
        } else mensaje("Error al obtener el estado.");
    }

    public void limpiar() {
        estado = null;
        txtNombre.setText("");
    }

    public boolean isNetworkingAvailable() {
        ConnectivityManager connmgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = connmgr.getActiveNetworkInfo();
        return netinfo != null && netinfo.isConnected();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGuardar:
                guardar();
                break;
            case R.id.btnLimpiar:
                limpiar();
                break;
            case R.id.btnListar:
                listar();
                break;
            case R.id.btnCerrar:
                System.exit(0);
                break;
        }
    }

    private void guardar() {
        if (!isNetworkingAvailable()) {
            mensaje("Error de conexión");
        } else if (!txtNombre.getText().toString().equals("")) {
            mensaje("El campo nombre es obligatorio.");
        } else if (estado == null) {
            estado = new Estado(txtNombre.getText().toString(), idMovil);
            php.insertarEstado(estado, insertReponse(true), errorResponse());
        } else if (hayCambios()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setTitle("Modificar estado");
            dialog.setMessage("¿Se modificará el estado " + estado.getNombre() + "?");
            dialog.setCancelable(false);
            dialog.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    php.actualizarEstado(estado, udtResponse(), errorResponse());
                }
            });
            dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) { } });
            dialog.show();
        }
    }

    private boolean hayCambios() {
        return !txtNombre.getText().toString().equals(estado.getNombre());
    }

    private Response.Listener<JSONObject> udtResponse() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray estados = response.getJSONArray("estados");
                    limpiar();
                    dbEstados.updateEstado(estado, estado.get_ID());
                    mensaje("Estado insertado!!!");
                } catch (Exception ex) {
                    mensaje("No se pudo insertar el estado.");
                }
            }
        };
    }

    private Response.Listener<JSONObject> insertReponse(final boolean msg) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray estados = response.getJSONArray("estados");
                    dbEstados.insertEstado(estado);
                    if (msg) {
                        limpiar();
                        mensaje("Estado insertado!!!");
                    }
                } catch (Exception ex) {
                    mensaje("No se pudo insertar el estado.");
                }
            }
        };
    }

    private Response.ErrorListener errorResponse() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mensaje("Ocurrio un error.");
            }
        };
    }

    private void listar() {
        limpiar();
        Intent i = new Intent(MainActivity.this, ListaActivity.class);
        startActivityForResult(i, Activity.RESULT_OK);
    }

    private void mensaje (String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private boolean esPrimeraVez () {
        SharedPreferences prefs = getBaseContext().getSharedPreferences("vacio", Context.MODE_PRIVATE);
        int id = prefs.getInt("id", 0);
        if (id == 0) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("id", id + 1);
            editor.commit();
            return true;
        }
        return false;
    }

    //Esta funcion mantiene los datos de lo 10 registros
    private ArrayList<Estado> getEstadosPrimeraVez() {
        ArrayList<Estado> estados = new ArrayList<>();
        estados.add(new Estado(1, "Aguascalientes", 1, idMovil));
        estados.add(new Estado(2, "Baja California Norte", 1, idMovil));
        estados.add(new Estado(3, "Baja California Sur", 1, idMovil));
        estados.add(new Estado(4, "Ciudad de México", 1, idMovil));
        estados.add(new Estado(5, "Morelos", 1, idMovil));
        estados.add(new Estado(6, "Jalisco", 1, idMovil));
        estados.add(new Estado(7, "Nuevo León", 1, idMovil));
        estados.add(new Estado(8, "Querétaro", 1, idMovil));
        estados.add(new Estado(9, "Chihuahua", 1, idMovil));
        estados.add(new Estado(10, "Sinaloa", 1, idMovil));
        return estados;
    }
}